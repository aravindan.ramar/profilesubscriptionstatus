package com.nutech.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.nutech.demo.bean.SubscriptionBean;
import com.nutech.demo.service.SubscriptionService;


/**
 * @author Aravindan
 *
 */
@RestController
@RequestMapping(value={"/subscription"})
public class SubscriptionController {
	@Autowired
	SubscriptionService subscriptionService;

   
    /**
     * @desc It will fetch the perticular profile subscription status using profile id
     * @param profileid
     * @return subscriptionStatus
     */
    @GetMapping(value = "/{profileId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SubscriptionBean> getSubscriptionStatus(@PathVariable("profileId") int profileid) {
        System.out.println("Fetching profile with id " + profileid);
        SubscriptionBean subscriptionStatus = subscriptionService.getSubscriptionStatus(profileid);
        if (subscriptionStatus == null) {
            return new ResponseEntity<SubscriptionBean>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<SubscriptionBean>(subscriptionStatus, HttpStatus.OK);
    }
    
    
    

	/**
	 * @desc It is used for create and update the subscription details
	 * @param subscriptionBean
	 * @return response
	 */
	@PostMapping(value="/subscribe",headers="Accept=application/json")
	 public ResponseEntity<String> createSubscription(@RequestBody SubscriptionBean subscriptionBean){
	     System.out.println("Subscribe User "+subscriptionBean.getUsername());
	     ResponseEntity<String> response = null;
	     SubscriptionBean subscriptionExist = subscriptionService.findByUsername(subscriptionBean.getUsername());
	     if(subscriptionExist != null) {
	    	 subscriptionExist.setEnddate(subscriptionBean.getEnddate());
	    	 subscriptionExist.setStatus("Active");
	    	 subscriptionService.renewSubscription(subscriptionExist);
	    	 response = new ResponseEntity<String>("updated", HttpStatus.OK);
	     }else {
	    	 subscriptionService.createSubscription(subscriptionBean);
	    	 response = new ResponseEntity<String>("created", HttpStatus.CREATED);
	     }
	     
	     return response;
	 }
	 
	
	/**
	 * @desc Get all profile status
	 * @return list
	 */
	@GetMapping(value="/getStatus", headers="Accept=application/json")
	 public List<SubscriptionBean> getAllSubscriptionStatus() {	 
	  List<SubscriptionBean> list=subscriptionService.getAllSubscriptionStatus();
	  return list;
	
	 }
}
