package com.nutech.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nutech.demo.bean.SubscriptionBean;
import com.nutech.demo.repository.SubscriptionRepository;

/**
 * @author Aravindan
 *
 */
@Service
@Transactional
public class SubscriptionServiceImpl implements SubscriptionService {
	@Autowired
	SubscriptionRepository subscriptionRepository;

	@Override
	public List<SubscriptionBean> getAllSubscriptionStatus() {
		return subscriptionRepository.getAllSubscriptionStatus();
	}
    
	@Override
	public SubscriptionBean getSubscriptionStatus(int id) {
		return subscriptionRepository.getSubscriptionStatus(id);
	}
	
	@Override
	public SubscriptionBean findByUsername(String username) {
		return subscriptionRepository.findByUsername(username);
	}

	@Override
	public void createSubscription(SubscriptionBean user) {
		subscriptionRepository.addSubscription(user);
	}

	@Override
	public void renewSubscription(SubscriptionBean subscriptionExist) {
		subscriptionRepository.renewSubscription(subscriptionExist);
	}

}
