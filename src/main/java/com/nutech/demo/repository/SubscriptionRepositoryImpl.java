package com.nutech.demo.repository;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nutech.demo.bean.SubscriptionBean;

/**
 * @author Aravindan
 *
 */
@Repository
public class SubscriptionRepositoryImpl implements SubscriptionRepository {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void addSubscription(SubscriptionBean user) {
		Session session = sessionFactory.getCurrentSession();
		session.save(user);
	}
	
	@Override
	public void renewSubscription(SubscriptionBean subBean) {
		Session session = sessionFactory.getCurrentSession();
		session.update(subBean);
	}

	@Override
	public List<SubscriptionBean> getAllSubscriptionStatus() {
		Session session = sessionFactory.getCurrentSession();
		@SuppressWarnings("unchecked")
		List<SubscriptionBean> list = session.createCriteria(SubscriptionBean.class).list();
		return list;
	}

	@Override
	public SubscriptionBean getSubscriptionStatus(int profileid) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from SubscriptionBean where profileid=:profileid");
		query.setParameter("profileid", profileid);
		return (SubscriptionBean) query.uniqueResult();
	}

	@Override
	public SubscriptionBean findByUsername(String username) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from SubscriptionBean where username=:username");
		query.setParameter("username", username);
		return (SubscriptionBean) query.uniqueResult();
	}

}
