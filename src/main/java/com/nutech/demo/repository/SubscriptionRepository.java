package com.nutech.demo.repository;

import java.util.List;

import com.nutech.demo.bean.SubscriptionBean;

/**
 * @author Aravindan
 *
 */
public interface SubscriptionRepository {
	public void addSubscription(SubscriptionBean user);
	public List<SubscriptionBean> getAllSubscriptionStatus();
	public SubscriptionBean getSubscriptionStatus(int id);
	public SubscriptionBean findByUsername(String username);
	public void renewSubscription(SubscriptionBean subscriptionExist);
	
}
